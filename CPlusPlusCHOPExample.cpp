/* Shared Use License: This file is owned by Derivative Inc. (Derivative)
* and can only be used, and/or modified for use, in conjunction with
* Derivative's TouchDesigner software, and only if you are a licensee who has
* accepted Derivative's TouchDesigner license or assignment agreement
* (which also govern the use of this file). You may share or redistribute
* a modified version of this file provided the following conditions are met:
*
* 1. The shared file or redistribution must retain the information set out
* above and this list of conditions.
* 2. Derivative's name (Derivative Inc.) or its trademarks may not be used
* to endorse or promote products derived from this file without specific
* prior written permission from Derivative.
*/

#include "CPlusPlusCHOPExample.h"
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <assert.h>
#include <iostream>
#include <PracticalSocketB.h>

//Cola y mutex
std::mutex dataWrite;
queue<std::vector<unsigned char>> cola;
std::atomic_bool thok(true);


UDPSocket sock;

extern "C"
{

	DLLEXPORT
		void
		FillCHOPPluginInfo(CHOP_PluginInfo* info)
	{
		info->apiVersion = CHOPCPlusPlusAPIVersion;
		info->customOPInfo.opType->setString("Kinet");
		info->customOPInfo.opLabel->setString("Kinet");
		info->customOPInfo.authorName->setString("ArmyRdz");
		info->customOPInfo.authorEmail->setString("jarodriguez@cocolab.mx");
		info->customOPInfo.minInputs = 1;
		info->customOPInfo.maxInputs = 1;
	}

	DLLEXPORT
		CHOP_CPlusPlusBase*
		CreateCHOPInstance(const OP_NodeInfo* info)
	{
		// Return a new instance of your class every time this is called.
		// It will be called once per CHOP that is using the .dll
		return new CPlusPlusCHOPExample(info);
	}

	DLLEXPORT
		void
		DestroyCHOPInstance(CHOP_CPlusPlusBase* instance)
	{
		thok = false;
		delete (CPlusPlusCHOPExample*)instance;
	}

};


CPlusPlusCHOPExample::CPlusPlusCHOPExample(const OP_NodeInfo* info) : myNodeInfo(info)
{
}

CPlusPlusCHOPExample::~CPlusPlusCHOPExample()
{

}

//se quit� el timeslice OJO
void
CPlusPlusCHOPExample::getGeneralInfo(CHOP_GeneralInfo* ginfo, const OP_Inputs* inputs, void* reserved1) // se quit� el timeslice
{
	// This will cause the node to cook every frame
	ginfo->cookEveryFrameIfAsked = true;

	// Note: To disable timeslicing you'll need to turn this off, as well as ensure that
	// getOutputInfo() returns true, and likely also set the info->numSamples to how many
	// samples you want to generate for this CHOP. Otherwise it'll take on length of the
	// input CHOP, which may be timesliced.
	ginfo->timeslice = false;

	ginfo->inputMatchIndex = 0;
}


//output Din�mico OJO
bool
CPlusPlusCHOPExample::getOutputInfo(CHOP_OutputInfo* info, const OP_Inputs* inputs, void* reserved1)
{
	// If there is an input connected, we are going to match it's channel names etc
	// otherwise we'll specify our own.

	if (inputs->getNumInputs() > 0)
	{
		const OP_CHOPInput* cinput = inputs->getInputCHOP(0);
		info->numChannels = cinput->numChannels;
		info->numSamples = cinput->numChannels;
		info->startIndex = 0;
		info->sampleRate = (float)cinput->sampleRate;

		return false;
	}
	else
	{
		info->numChannels = 1;
		info->numSamples = 1;
		info->startIndex = 0;
		info->sampleRate = 120;
		return true;
	}
} //output din�mico OJO

void
CPlusPlusCHOPExample::getChannelName(int32_t index, OP_String* name, const OP_Inputs* inputs, void* reserved1)
{
	name->setString("chan1");
}

void
CPlusPlusCHOPExample::execute(CHOP_Output* output,
	const OP_Inputs* inputs,
	void* reserved)
{
	std::string servAddress = inputs->getParString("Address"); // First arg: multicast address
	int servPort = inputs->getParInt("Port"); // Second arg: port
	int active = inputs->getParInt("Active"); // Second arg: port

	if (inputs->getNumInputs() > 0 && active)
	{

		const OP_CHOPInput* cinput1 = inputs->getInputCHOP(0);

		const OP_DATInput* cinput = inputs->getParDAT("Dat");
		int version = inputs->getParInt("Version");


		/*
	   unsigned char kinet_header[12] = {
	   0x04, 0x01, 0xDC, 0x4A,       // magic number
	   0x02, 0x00,                   // KiNet version
	   0x08, 0x01,                   // Packet type
	   0x00, 0x00, 0x00, 0x00};      // Unused, sequence number
	   unsigned char kinet_PORT_header[12] = {
	   0xFF, 0xFF, 0xFF, 0xFF,       // universe, FFFF FFFF is "don't care"
	   0x01,                         // Port on device controller
	   0x00,                         // pad, unused
	   0x01, 0x00,                   // Flags
	   0x96, 0x00,                   // Set length, setting to 150 (send all
   values)
	   0x00, 0x00};                  // start code
	   */



		int ind = 0;

		if (inputs->getParDAT("Dat")) {

			double v = 0.0f;  //quitar v

			switch (version)
			{
			case 0:		// sine
				for (int i = 0; i < output->numChannels; i++) {
					int outNumPort = atoi(cinput->getCell(i, 0));
					int numChan = atoi(cinput->getCell(i, 1));
					int universe = atoi(cinput->getCell(i, 2));

					if (outNumPort && numChan && universe) {
						std::vector<unsigned char> datagram{ 0x04, 0x01, 0xdc, 0x4a, 0x01, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, (unsigned char)outNumPort, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x00,(unsigned char)universe };
						std::vector<unsigned char> dataLeds;
						for (int j = 0; j < output->numSamples; j++) {
							const OP_CHOPInput* cinput = inputs->getInputCHOP(0);
							output->channels[i][j] = float(cinput->getChannelData(i)[j]);

							if (j < numChan) {
								dataLeds.push_back((unsigned char)cinput->getChannelData(i)[j]);
							}
						}
						datagram.insert(datagram.end(), dataLeds.begin(), dataLeds.end());

						dataWrite.lock();
						cola.push(datagram);
						dataWrite.unlock();

					//	sock.sendTo(datagram.data(), datagram.size(), servAddress, servPort);
					//	std::cout << "vectorCreado   tama�oN: " << datagram.size() << " =  " << dataLeds.size() << "\n";
					}
					else {
						break;
					}
				}
				break;

			case 1:		// square
				for (int i = 0; i < output->numChannels; i++) {
					int outNumPort = atoi(cinput->getCell(i, 0));
					int numChan = atoi(cinput->getCell(i, 1));
					int universe = atoi(cinput->getCell(i, 2));

					if (outNumPort && numChan && universe) {
						std::vector<unsigned char> datagram{ 0x04, 0x01, 0xdc, 0x4a, 0x02, 0x00, 0x08, 0x01, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, (unsigned char)outNumPort, 0x00, 0x01, 0x00,(unsigned char)numChan, 0x00, 0x00,(unsigned char)universe };
						std::vector<unsigned char> dataLeds;
						for (int j = 0; j < output->numSamples; j++) {
							const OP_CHOPInput* cinput = inputs->getInputCHOP(0);
							output->channels[i][j] = float(cinput->getChannelData(i)[j]);

							if (j < numChan) {
								dataLeds.push_back((unsigned char)cinput->getChannelData(i)[j]);
							}
						}
						datagram.insert(datagram.end(), dataLeds.begin(), dataLeds.end());

						dataWrite.lock();
						cola.push(datagram);
						dataWrite.unlock();

					//	sock.sendTo(datagram.data(), datagram.size(), servAddress, servPort);
					//	std::cout << "vectorCreado   tama�oM: " << datagram.size() << " =  " << dataLeds.size() << "\n";
					}
					else {
						break;
					}
				}
				break;
			}
		}


	}
	else // If not input is connected, 0
	{
		for (int i = 0; i < output->numChannels; i++)
		{
			for (int j = 0; j < output->numSamples; j++)
			{
				output->channels[i][j] = 0;
			}

		}

	}


	if (estado == 0) {

		std::thread t1([servPort, servAddress]() {
			while (thok.load()) {
				if (!cola.empty()) {
					dataWrite.lock();
					std::vector<unsigned char> dataLedsSend = cola.front();
					
					cola.pop();
					dataWrite.unlock();

					sock.sendTo(dataLedsSend.data(), dataLedsSend.size(), servAddress, servPort);
					//unsigned char entrance = dataLedsSend.at(16);
					//unsigned char entrance2 = dataLedsSend.at(20);
					//	std::cout << "vectorCreado   salida" << (int)entrance << " canales  " << (int)entrance2<< "\n";

				}
			}
			});

		estado = 1;
		t1.detach();

	}



}

int32_t
CPlusPlusCHOPExample::getNumInfoCHOPChans(void* reserved1)
{
	// We return the number of channel we want to output to any Info CHOP
	// connected to the CHOP. In this example we are just going to send one channel.
	return 2;
}

void
CPlusPlusCHOPExample::getInfoCHOPChan(int32_t index,
	OP_InfoCHOPChan* chan,
	void* reserved1)
{
	// This function will be called once for each channel we said we'd want to return
	// In this example it'll only be called once.

	if (index == 0)
	{
		chan->name->setString("executeCount");
		chan->value = (float)myExecuteCount;
	}

	if (index == 1)
	{
		chan->name->setString("offset");
		chan->value = (float)myOffset;
	}
}

bool
CPlusPlusCHOPExample::getInfoDATSize(OP_InfoDATSize* infoSize, void* reserved1)
{
	infoSize->rows = 2;
	infoSize->cols = 2;
	// Setting this to false means we'll be assigning values to the table
	// one row at a time. True means we'll do it one column at a time.
	infoSize->byColumn = false;
	return true;
}

void
CPlusPlusCHOPExample::getInfoDATEntries(int32_t index,
	int32_t nEntries,
	OP_InfoDATEntries* entries,
	void* reserved1)
{
	char tempBuffer[4096];

	if (index == 0)
	{
		// Set the value for the first column
		entries->values[0]->setString("executeCount");

		// Set the value for the second column
#ifdef _WIN32
		sprintf_s(tempBuffer, "%d", myExecuteCount);
#else // macOS
		snprintf(tempBuffer, sizeof(tempBuffer), "%d", myExecuteCount);
#endif
		entries->values[1]->setString(tempBuffer);
	}

	if (index == 1)
	{
		// Set the value for the first column
		entries->values[0]->setString("offset");

		// Set the value for the second column
#ifdef _WIN32
		sprintf_s(tempBuffer, "%g", myOffset);
#else // macOS
		snprintf(tempBuffer, sizeof(tempBuffer), "%g", myOffset);
#endif
		entries->values[1]->setString(tempBuffer);
	}
}

void
CPlusPlusCHOPExample::setupParameters(OP_ParameterManager* manager, void* reserved1)
{



	// address
	{
		OP_NumericParameter	np;

		np.name = "Active";
		np.label = "Active";
		np.defaultValues[0] = 0;

		OP_ParAppendResult res = manager->appendToggle(np);
		assert(res == OP_ParAppendResult::Success);
	}


	// CHOP
	{
		OP_StringParameter	np;

		np.name = "Dat";
		np.label = "DAT";

		OP_ParAppendResult res = manager->appendDAT(np);
		assert(res == OP_ParAppendResult::Success);
	}



	// address
	{
		OP_StringParameter	np;

		np.name = "Address";
		np.label = "Address";
		np.defaultValue = "127.0.0.1";

		OP_ParAppendResult res = manager->appendString(np);
		assert(res == OP_ParAppendResult::Success);
	}

	// Port
	{
		OP_NumericParameter	np;

		np.name = "Port";
		np.label = "Port";
		np.defaultValues[0] = 5500;
		np.minSliders[0] = 5490;
		np.maxSliders[0] = 5510;

		OP_ParAppendResult res = manager->appendInt(np);
		assert(res == OP_ParAppendResult::Success);
	}

	// version
	{
		OP_StringParameter	sp;

		sp.name = "Version";
		sp.label = "Version";

		sp.defaultValue = "V2";

		const char* names[] = { "V1", "V2" };
		const char* labels[] = { "V1", "V2" };

		OP_ParAppendResult res = manager->appendMenu(sp, 2, names, labels);
		assert(res == OP_ParAppendResult::Success);
	}

	// pulse
	{
		OP_NumericParameter	np;

		np.name = "Reset";
		np.label = "Reset";

		OP_ParAppendResult res = manager->appendPulse(np);
		assert(res == OP_ParAppendResult::Success);
	}

}

void
CPlusPlusCHOPExample::pulsePressed(const char* name, void* reserved1)
{
	if (!strcmp(name, "Reset"))
	{
		thok = false;
		estado = 0;
		Sleep(100);
		thok = true;
	}
}

